## UNO score counter

UNO game score counter is a console app written in C/C++ using _ncurses_ framework. Windows version uses _PDCurses_ which doesn't has _Forms_.
The extra file "firework" is a joke! but is **necessary** to avoid crashes. 

### Screenshot
![Screenshot of the application](https://photos.app.goo.gl/c7aV6sHdDUmA7HVu7)